sgmllib3k (1.0.0-5+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Mon, 10 Mar 2025 00:26:10 +0000

sgmllib3k (1.0.0-5) unstable; urgency=medium

  * Adopt package inside DPT (Closes: #975620)

  [ Debian Janitor ]
  * Avoid pypi.org in Homepage field.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.1, no changes needed.

 -- Alexandre Detiste <tchet@debian.org>  Thu, 22 Aug 2024 13:34:43 +0200

sgmllib3k (1.0.0-4+apertis0) apertis; urgency=medium

  * Import from Debian bookworm.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Tue, 25 Apr 2023 19:46:38 +0530

sgmllib3k (1.0.0-4) unstable; urgency=low

  [ Debian Janitor ]
  * Set debhelper-compat version in Build-Depends.
  * Set field Upstream-Name in debian/copyright.
  * Update standards version to 4.5.0, no changes needed.

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address
  * debian/compat
    - drop in favor of d/control

  [ Debian Janitor ]
  * Update watch file format version to 4.
  * Update standards version to 4.5.1, no changes needed.

 -- Sandro Tosi <morph@debian.org>  Mon, 06 Jun 2022 23:10:14 -0400

sgmllib3k (1.0.0-3) unstable; urgency=low

  * debian/tests/control, debian/tests/pkg-python/import-name:
    Use autodep8 instead of manually specifying a test.

 -- Mikhail Gusarov <dottedmag@debian.org>  Sun, 05 Jan 2020 01:00:23 +0100

sgmllib3k (1.0.0-2) unstable; urgency=low

  * debian/tests/control: Override default testsuite run by autopkgtest.

 -- Mikhail Gusarov <dottedmag@debian.org>  Wed, 01 Jan 2020 15:40:52 +0100

sgmllib3k (1.0.0-1) unstable; urgency=medium

  * Initial Release (Closes: #946031)

 -- Mikhail Gusarov <dottedmag@debian.org>  Tue, 03 Dec 2019 08:45:53 +0100
